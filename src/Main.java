package com.company;

import java.math.BigInteger;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        try {
            switch (args[0]) {
                case "1.1":
                    problem1_1();
                    break;
                case "1.2":
                    problem1_2();
                    break;
                case "1.3":
                    problem1_3();
                    break;
                case "1.4":
                    problem1_4();
                    break;
                case "2":
                    problem2();
                    break;
                case "3":
                    problem3();
                    break;
                case "4":
                    problem4();
                    break;
                default:
                    System.out.println("Run a program by passing an argument (1.1, 1.2, 1.3, 1.4, 2, 3 or 4)");
                    break;
            }
        } catch(ArrayIndexOutOfBoundsException ex) {
            System.out.println("Run a program by passing an argument (1.1, 1.2, 1.3, 1.4, 2, 3 or 4)");
        }
    }


    //uses BinaryString.java
    public static void problem1_1() {
        BinaryString b = new BinaryString("011100110100100110100111010001110101100100100100010111011101");

        // print the binary string first
        System.out.println(b.toString());

        //decode the string to 1s and 0s
        System.out.println(b.decode());

        //encode this string of characters
        String s = BinaryString.toInt("The Quick Brown Fox Jumps Over The Lazy Dog? (.,)");
        System.out.println(s);

        //decode it back
        System.out.println(new BinaryString(s).decode());

    }

    //uses DES.java
    public static void problem1_2() {
        //     1011 0101
        //     1234 5678
        //1011 1101 0101
        //1234 3456 5678
        System.out.println("Final Output: " + DES.f("10110101", "110011001100", true));
    }
    //uses primarily BlockCipher.java
    //dependencies: DES.java and BinaryString.java for xor, DES.f(), BinaryString().decode()
    public static void problem1_3() {
        System.out.println("Enter some plaintext: ");
        Scanner sc = new Scanner(System.in);
        String plain = sc.nextLine();
        System.out.println("Enter a 24 character key: ");
        String key = sc.nextLine();
        String s = BlockCipher.encrypt(plain, key, false);
        System.out.println("L2R2: " + s);
        while(s.length() % 5 != 0) {//padding
            s += "0";
        }
        System.out.println("in Z32: " + new BinaryString(s).decode());
    }

    //uses primarily BlockCipher.java
    //dependencies: DES.java and BinaryString.java for xor, DES.f(), BinaryString().decode()
    public static void problem1_4() {
        String s = BlockCipher.encrypt("how do you like computer science", "101101010010100101101011", false);
        System.out.println("L2R2: " + s);
        System.out.println("in Z32: " + new BinaryString(s).decode());
    }

    //uses BlockCipher.java
    //dependencies: DES.java and BinaryString.java for xor, DES.f(), BinaryString().decode()
    public static void problem2() {
        String s = BlockCipher.encrypt("how do you like computer science", "101101010010100101101011", false);
        System.out.println("encrypted: " + s);
        s = new BinaryString(s).decode();
        System.out.println("in Z32: " + s);
        String d = BlockCipher.decrypt(s, "101101010010100101101011", false);
        System.out.println("decrypted: " + d);
        System.out.println("in Z32: " + new BinaryString(d).decode());
    }

    //uses BlockCipher.java
    public static void problem3() {
        String s = "cryptography is an important tool for network security. but there are other issues for network security.";
        String key = "101101010010100101101011"; //same as problem 1
        String cbc = BlockCipher.CBCencrypt(s, key, false, 0);
        System.out.println("Plaintext: " + s);
        System.out.println("ciphertext: " + cbc);
        while(cbc.length() % 5 != 0) {
            cbc = cbc + "0";
        }
        System.out.println("in Z32: " + new BinaryString(cbc).decode());
    }

    //uses MillerRabin.java
    public static void problem4() {
        System.out.println("result: " + MillerRabin.primality(BigInteger.valueOf(61), 3, 6221));
        System.out.println("result: " + MillerRabin.primality(new BigInteger("900307"), 5, 1));
    }




}
