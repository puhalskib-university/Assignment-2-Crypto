package com.company;

public class BinaryString {

    public String binaryString;

    /*
    *   Constructor
     */

    BinaryString(String str) {
        if(str.length() % 5 != 0) {
            throw new IllegalStateException("Illegal binaryString length: " + str.length());
        }
        binaryString = str;
    }

    /*
     *  Private Methods
     */

    private static String integerToBinary(int a) {
        String s = "";
        while(a > 0) {
            if(a % 2 == 0) {
                s = "0" + s;
            } else {
                s = "1" + s;
            }
            a = a / 2;
        }
        return s;
    }

    private static String integerToBinary(int a, int totalChar) {
        String s = "";
        for(int i = 0; i < totalChar; i++) {
            if(a % 2 == 0) {
                s = "0" + s;
            } else {
                s = "1" + s;
            }
            a = a / 2;
        }
        return s;
    }

    private int binaryStringToInteger(String a) {
        int n = 0;
        int j = a.length() - 1;
        for(int i = 0; i < a.length(); i++) {
            if(a.charAt(i) == '1') {
                n += Math.pow(2, j);
            }
            j--;
        }
        return n;
    }

    private static int charToInt_Z32(char c) {
        //convert to upper case
        if(c >= 'a' && c <= 'z') {
            c = (char) (c - 'a' + 'A');
        }
        // return corresponding integer
        switch(c) {
            case ' ':
                return 26;
            case '.':
                return 27;
            case ',':
                return 28;
            case '?':
                return 29;
            case '(':
                return 30;
            case ')':
                return 31;
            default:
                //should be a letter
                if(c >= 'A' && c <= 'Z') {
                    return c - 'A';
                } else {
                    throw new IllegalStateException("Unexpected value: " + c);
                }
        }
    }

    private char intToChar_Z32(int i) {
        // return corresponding integer
        switch(i) {
            case 26:
                return ' ';
            case 27:
                return '.';
            case 28:
                return ',';
            case 29:
                return '?';
            case 30:
                return '(';
            case 31:
                return ')';
            default:
                //should be a letter
                if(i <= 25 && i >= 0) {
                    return (char) (i + 'A');
                } else {
                    throw new IllegalStateException("Unexpected value: " + i);
                }
        }
    }

    /*
     * Public method
     */


    //decode the binary string of 1s and 0s to an actual string of letters in Z32
    public String decode() {
        String bnStr = binaryString;
        String str = "";
        while(bnStr.length() > 0) {
            str += intToChar_Z32(binaryStringToInteger(bnStr.substring(0, 5)));
            bnStr = bnStr.substring(5);
        }
        return str;
    }

    @Override
    public String toString() {
        return binaryString;
    }

    /*
     * Static Methods
     */

    //convert a normal string in Z32 to a string of 1s and 0s
    public static String toInt(String s) {
        String str = "";
        for(char c : s.toCharArray()) {
            str += integerToBinary(charToInt_Z32(c), 5);
        }
        return str;
    }




}
