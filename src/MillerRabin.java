package com.company;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Random;

public class MillerRabin {

    public static String primality(BigInteger n, int k, int seed) {
        Random rand = new Random(seed);

        BigInteger num = n.subtract(BigInteger.ONE); // num = n - 1
        BigInteger d;
        int r;
        for(int i = 0; true; i++) {
            if(!(num.remainder(BigDecimal.valueOf(Math.pow(2,i)).toBigInteger()).equals(BigInteger.ZERO))) { // num % Math.pow(2,i) != 0
                if(i > 0) {
                    i--;
                }
                r = i;
                d = num.divide(BigDecimal.valueOf(Math.pow(2,i)).toBigInteger());
                break;
            }
        }
        System.out.println(n + " = 2^" + r + " * " + d + " + 1");

        for(int i = 0; i < k; i++) {
            //https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test#Miller%E2%80%93Rabin_test
            //get a random biginteger between [2, n-2]
            BigInteger a;
            do {
                a = new BigInteger((num.subtract(BigInteger.ONE)).bitLength(), rand);
            } while (a.compareTo((num.subtract(BigInteger.ONE))) >= 0 || a.compareTo(new BigInteger("2")) < 0 ); // while(a >= n - 2)
            System.out.println("a = " + a);

            //BigInteger x = (a.pow(d)).mod(n);
            BigInteger x = a.modPow(d,n);

            if(x.equals(BigInteger.ONE) || x.equals(num)) {
                continue;
            }

            for(int j = 0; j < r - 1; j++) {
                //x = (x.pow(2)).mod(n);
                x = x.modPow(BigInteger.valueOf(2),n);
                if(x.equals(num)){
                    continue;
                }
            }
            return "composite";

        }
        return "probably prime";

    }
}
