package com.company;

public class DES {

    public static String f(String plaintext, String k, boolean debug) {
        if(plaintext.length() != 8 || k.length() != 12) {
            throw new IllegalStateException("Illegal length state: (plaintext)" + plaintext.length() + " (key)" + k.length());
        }
        if(debug) {
            System.out.println("***Running f(" + plaintext + ", " + k + "***");
            System.out.println("testing e: " + e(plaintext) + "\n");
        }

        String e = e(plaintext);

        if(debug) {
            System.out.println("testing xor: " + e);
            System.out.println("             " + k);
            System.out.println("           = " + xor(e, k));
        }

        String x = xor(e, k);


        //dividing the string down the center (6bits, 6bits)
        String B1 = x.substring(0, 6);
        String B2 = x.substring(6);

        if(debug) System.out.println("B1: " + B1 + "\n" + "B2: " + B2 + "\n");

        // S Boxes 1 and 2
        int[][] S1 = {
                {15,1,8,14,6,11,3,4,9,7,2,13,12,0,5,10},
                {3,13,4,7,15,2,8,14,12,0,1,10,6,9,11,5},
                {0,14,7,11,10,4,13,1,5,8,12,6,9,3,2,15},
                {13,8,10,1,3,15,4,2,11,6,7,12,0,5,14,9}
        };
        int[][] S2 = {
                {7,13,14,3,0,6,9,10,1,2,8,5,11,12,4,15},
                {13,8,11,5,6,15,0,3,4,7,2,12,1,10,14,9},
                {10,6,9,0,12,11,7,13,15,1,3,14,5,2,8,4},
                {3,15,0,6,10,1,13,8,9,4,5,11,12,7,2,14}
        };
        //Using bits 1 and 6 to determine row
        //Using bits 2,3,4,5 to determine column

        //For B1
        int B1row = binaryStringToInteger("" + B1.charAt(0) + B1.charAt(5) ); //need to put empty string to use string concat
        int B1col = binaryStringToInteger("" + B1.charAt(1) + B1.charAt(2) + B1.charAt(3) + B1.charAt(4));

        if(debug) {
            System.out.println("B1row: " + B1.charAt(0) + B1.charAt(5) + " or " + B1row);
            System.out.println("B1col: " + B1.charAt(1) + B1.charAt(2) + B1.charAt(3) + B1.charAt(4) +  " or " + B1col);
        }


        //For B2
        int B2row = binaryStringToInteger("" + B2.charAt(0) + B2.charAt(5) );
        int B2col = binaryStringToInteger("" + B2.charAt(1) + B2.charAt(2) + B2.charAt(3) + B2.charAt(4));

        if(debug) {
            System.out.println("B2row: " + B2.charAt(0) + B2.charAt(5) + " or " + B2row);
            System.out.println("B2col: " + B2.charAt(1) + B2.charAt(2) + B2.charAt(3) + B2.charAt(4) + " or " + B2col + "\n");
        }

        //Get at index of S boxes, B1 -> S1, B2 -> S2
        //pad the strings to have 4 characters

        String B1str = integerToBinary(S1[B1row][B1col], 4);
        if(debug) System.out.println("testing S1: " + S1[B1row][B1col]);

        String B2str = integerToBinary(S2[B2row][B2col], 4);
        if(debug) System.out.println("testing S2: " + S2[B2row][B2col]);

        return B1str + B2str;
    }

    private static String e(String plaintext) {
        String s = "";
        int j = 0;
        for(int i = 0; i < 12; i++) {
            s += plaintext.charAt(j);
            if(i == 3 || i == 7) {
                j--;
            } else {
                j++;
            }
        }
        return s;
    }

    public static String xor(String a, String b) {
        String s = "";
        if(a.length() != b.length()) {
            throw new IllegalStateException("Illegal length state: (plaintext)" + a.length() + " (key)" + b.length());
        }
        int x = binaryStringToInteger(a);
        int y = binaryStringToInteger(b);

        return integerToBinary(x^y, a.length());
    }

    private static int binaryStringToInteger(String a) {
        int n = 0;
        int j = a.length() - 1;
        for(int i = 0; i < a.length(); i++) {
            if(a.charAt(i) == '1') {
                n += Math.pow(2, j);
            }
            j--;
        }
        return n;
    }

    private static String integerToBinary(int a, int totalChar) {
        String s = "";
        for(int i = 0; i < totalChar; i++) {
            if(a % 2 == 0) {
                s = "0" + s;
            } else {
                s = "1" + s;
            }
            a = a / 2;
        }
        return s;
    }
}
