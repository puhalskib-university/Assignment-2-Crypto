package com.company;

import java.util.Random;

public class BlockCipher {

    public static String encrypt(String plaintext, String key, boolean debug) {
        if(key.length() != 24) {
            throw new IllegalStateException("Illegal key length: " + key.length());
        }
        String bitString = BinaryString.toInt(plaintext);
        if(debug) {
            System.out.print("           ");
            for(int i = 0; i < plaintext.length(); i++) {
                System.out.print("  " + plaintext.charAt(i) + "   ");
            }
            System.out.print("\nbitstring: ");
            for(int i = 0; i < (bitString.length() / 5); i++) {
                System.out.print(bitString.substring(i*5, (i*5)+5) + " ");
            }
            System.out.println("\nNumber of blocks: " + (int) (Math.ceil(bitString.length() / 16.0)));
        }

        //array of string blocks
        String[] strArr = new String[(int)(Math.ceil(bitString.length()/16.0))];

        for(int i = 0; i < strArr.length; i++) {
            try {
                strArr[i] = bitString.substring(0, 16);
                bitString = bitString.substring(16);
            } catch(StringIndexOutOfBoundsException ex){
                //add padding to the end of the last substring with 0s
                strArr[i] = bitString;
                for(int j = 0; j < 16 - bitString.length(); j++) {
                    strArr[i] += "0";
                }
            }
        }
//        if(debug) {
//            System.out.println("String blocks: ");
//            for(int i = 0; i < strArr.length; i++) {
//                System.out.println(i + "\t" + strArr[i]);
//            }
//        }

        //divide each into 2 strings
        String[][] LR = new String[strArr.length][2];
        for(int i = 0; i < LR.length; i++) {
            LR[i][0] = strArr[i].substring(0, 8);
            LR[i][1] = strArr[i].substring(8);
        }

        if(debug) {
            System.out.println("String blocks: ");
            for(int i = 0; i < LR.length; i++) {
                System.out.println(i + "\tL" + i + " " + LR[i][0] + "  R" + i + " " + LR[i][1]);
            }
        }

        String outputString = "";
        for(String[] s : LR) {

            for (int i = 1; i <= 2; i++) {
                String tmpL = s[0];
                s[0] = s[1]; // Li = Ri-1
                s[1] = DES.xor(tmpL, DES.f(s[1], key.substring((i - 1) * 12, ((i - 1) * 12) + 12), debug)); // Ri = Li-1 xor f(Ri-1, Ki)
            }
            outputString += s[0] + s[1];
        }



        return outputString;

    }

    public static String decrypt(String ciphertext, String key, boolean debug) {
        if(key.length() != 24) {
            throw new IllegalStateException("Illegal key length: " + key.length());
        }
        String bitString = BinaryString.toInt(ciphertext);
        if(debug) {
            System.out.print("           ");
            for(int i = 0; i < ciphertext.length(); i++) {
                System.out.print("  " + ciphertext.charAt(i) + "   ");
            }
            System.out.print("\nEncrypted bitstring: ");
            for(int i = 0; i < (bitString.length() / 5); i++) {
                System.out.print(bitString.substring(i*5, (i*5)+5) + " ");
            }
            System.out.println("\nNumber of blocks: " + (int) (Math.ceil(bitString.length() / 16.0)));
        }

        //array of string blocks
        String[] strArr = new String[(int)(Math.ceil(bitString.length()/16.0))];

        for(int i = 0; i < strArr.length; i++) {
            try {
                strArr[i] = bitString.substring(0, 16);
                bitString = bitString.substring(16);
            } catch(StringIndexOutOfBoundsException ex){
                //add padding to the end of the last substring with 0s
                strArr[i] = bitString;
                for(int j = 0; j < 16 - bitString.length(); j++) {
                    strArr[i] += "0";
                }
            }
        }

        //divide each into 2 strings
        String[][] LR = new String[strArr.length][2];
        for(int i = 0; i < LR.length; i++) {
            LR[i][0] = strArr[i].substring(0, 8);
            LR[i][1] = strArr[i].substring(8);
        }

        if(debug) {
            System.out.println("String blocks: ");
            for(int i = 0; i < LR.length; i++) {
                System.out.println(i + "\tL" + i + " " + LR[i][0] + "  R" + i + " " + LR[i][1]);
            }
        }

        String outputString = "";
        for(String[] s : LR) {

            for (int i = 1; i >= 0; i--) {
                String tmpR = s[1];
                s[1] = s[0]; // Ri-1 = Li
                s[0] = DES.xor(tmpR, DES.f(s[1], key.substring(i * 12, (i * 12) + 12), debug)); // Li-1 = Ri xor f(Ri-1, Ki)
            }
            outputString += s[0] + s[1];
        }



        return outputString;

    }


    public static String CBCencrypt(String plaintext, String key, boolean debug, int seed) {
        if(key.length() != 24) {
            throw new IllegalStateException("Illegal key length: " + key.length());
        }
        String bitString = BinaryString.toInt(plaintext);

        //array of string blocks
        String[] strArr = new String[(int)(Math.ceil(bitString.length()/16.0))];

        for(int i = 0; i < strArr.length; i++) {
            try {
                strArr[i] = bitString.substring(0, 16);
                bitString = bitString.substring(16);
            } catch(StringIndexOutOfBoundsException ex){
                //add padding to the end of the last substring with 0s
                strArr[i] = bitString;
                for(int j = 0; j < 16 - bitString.length(); j++) {
                    strArr[i] += "0";
                }
            }
        }

        //chain ciphers
        String[] cipherStrArr = new String[strArr.length];
        cipherStrArr[0] = "";

        //generate random IV on the first index
        Random rand = new Random(seed);
        while(cipherStrArr[0].length() < 16) {
            if(rand.nextBoolean()) {
                cipherStrArr[0] += "1";
            } else {
                cipherStrArr[0] += "0";
            }
        }

        for(int i = 0; i < strArr.length; i++) {
            //cipherStrArr[i] = BlockCipher.encrypt(DES.xor(cipherStrArr[i-1], strArr[i]), key , debug);

            //encrypt block
            String block;
            if(i != 0) {
                block = DES.xor(cipherStrArr[i - 1], strArr[i]);
            } else {
                block = cipherStrArr[0];
            }

            String block1 = block.substring(0,block.length()/2);
            String block2 = block.substring(block.length()/2);
            for (int j = 1; j <= 2; j++) {
                String tmpL = block1;
                block1 = block2; // Li = Ri-1
                block2 = DES.xor(tmpL, DES.f(block2, key.substring((j - 1) * 12, ((j - 1) * 12) + 12), debug)); // Ri = Li-1 xor f(Ri-1, Ki)
            }
            cipherStrArr[i] = block1 + block2;
        }

        //output
        String outStr = "";
        for(String s : cipherStrArr) {
            outStr += s;
        }

        return outStr;
    }


}
