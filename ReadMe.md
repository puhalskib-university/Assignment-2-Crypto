# ReadMe

COMP-4476-WDE Cryptography Network Security

Ben Puhalski

**Assignment 2**

# Table of Contents

1. [Problem 1](#problem-1)
   1. [Part 1](#part-1)
   2. [Part 2](#part-2)
   3. [Part 3](#part-3)
   4. [Part 4](#part-4)
2. [Problem 2](#problem-2)
3. [Problem 3](#problem-3)
4. [Problem 4](#problem-4)
5. [Running the program](#Running-the-program)

# Problem 1

## Part 1

Code in BinaryString.java

-----

```java
//convert a normal string in Z32 to a string of 1s and 0s
    public static String toInt(String s) {
        String str = "";
        for(char c : s.toCharArray()) {
            str += integerToBinary(charToInt_Z32(c), 5);
        }
        return str;
    }
```

Converts from characters to binary string of 1s and 0s. Uses utility methods to simplify code.

```java
    //decode the binary string of 1s and 0s to an actual string of letters in Z32
    public String decode() {
        String bnStr = binaryString;
        String str = "";
        while(bnStr.length() > 0) {
            str += intToChar_Z32(binaryStringToInteger(bnStr.substring(0, 5)));
            bnStr = bnStr.substring(5);
        }
        return str;
    }
```

Converts binary string to letters in Z32

## Part 2

Code in DES.java

-----

first converts plaintext to have the order: 0,1,2,3,2,3,4,5,4,5,6,7 a 12 length string.

```java
    private static String e(String plaintext) {
        String s = "";
        int j = 0;
        for(int i = 0; i < 12; i++) {
            s += plaintext.charAt(j);
            if(i == 3 || i == 7) {
                j--;
            } else {
                j++;
            }
        }
        return s;
    }
```

Then xor with the key. Uses ^ to xor to integers then converts back to string.

```java
    public static String xor(String a, String b) {
        String s = "";
        if(a.length() != b.length()) {
            throw new IllegalStateException("Illegal length state: (plaintext)" + 
                      a.length() + " (key)" + b.length());
        }
        int x = binaryStringToInteger(a);
        int y = binaryStringToInteger(b);

        return integerToBinary(x^y, a.length());
    }
```

Then uses the binary string to get values from the S-boxes. 

```java
String B1str = integerToBinary(S1[B1row][B1col], 4);
String B2str = integerToBinary(S2[B2row][B2col], 4);

return B1str + B2str;
```

## Part 3

Code in BlockCipher.java

--------

First divides into 16 bit chunks then those chunks into two 8 bit chunks.

```java
//array of string blocks
        String[] strArr = new String[(int)(Math.ceil(bitString.length()/16.0))];

        for(int i = 0; i < strArr.length; i++) {
            try {
                strArr[i] = bitString.substring(0, 16);
                bitString = bitString.substring(16);
            } catch(StringIndexOutOfBoundsException ex){
                //add padding to the end of the last substring with 0s
                strArr[i] = bitString;
                for(int j = 0; j < 16 - bitString.length(); j++) {
                    strArr[i] += "0";
                }
            }
        }

        //divide each into 2 strings
        String[][] LR = new String[strArr.length][2];
        for(int i = 0; i < LR.length; i++) {
            LR[i][0] = strArr[i].substring(0, 8);
            LR[i][1] = strArr[i].substring(8);
        }
```

Then, using the function described using 2 iterations and f is defined in DES.java for 1<= i <= 2.

<img src="figures/BlockCipherEncrypt.png" style="zoom: 67%;" />

```java
        String[][] LR = new String[strArr.length][2];
        for(int i = 0; i < LR.length; i++) {
            LR[i][0] = strArr[i].substring(0, 8);
            LR[i][1] = strArr[i].substring(8);
        }

        String outputString = "";
        for(String[] s : LR) {

            for (int i = 1; i <= 2; i++) {
                String tmpL = s[0];
                s[0] = s[1]; // Li = Ri-1
                s[1] = DES.xor(tmpL, DES.f(s[1], key.substring((i - 1) * 12, ((i - 1) * 
                			12) + 12), debug)); // Ri = Li-1 xor f(Ri-1, Ki)
            }
            outputString += s[0] + s[1];
        }
```

## Part 4

<img src="figures/outputProblem1_4.png"/>

```java
    public static void problem1_4() {
        String s = BlockCipher.encrypt("how do you like computer science", 
                                       "101101010010100101101011", false);
        System.out.println("L2R2: " + s);
        System.out.println("in Z32: " + new BinaryString(s).decode());
    }
```

The false is used for debug information. Use it for more detailed System.out information.

# Problem 2

Code in BlockCipher.java

---------

Code is identical for dividing the plaintext into blocks. The following is the differing code for decryption.

```java
String outputString = "";
for(String[] s : LR) {

    for (int i = 1; i >= 0; i--) {
        String tmpR = s[1];
        s[1] = s[0]; // Ri-1 = Li
        s[0] = DES.xor(tmpR, DES.f(s[1], key.substring(i * 12, (i * 12) + 12), debug)); 
        						// Li-1 = Ri xor f(Ri-1, Ki)
    }
    outputString += s[0] + s[1];
}
```

s[0] represents L and s[1] represents R.

DES.xor is the utility function from problem 1. DES.f is the function f from problem 1.

# Problem 3

Code in BlockCipher.java

---

<img src="figures/chainCipher.png" style="zoom:50%;" />

Uses the above figure as the method of CBC chain ciphering in the following implementation.

```java
        for(int i = 0; i < strArr.length; i++) {
            //cipherStrArr[i] = BlockCipher.encrypt(DES.xor(cipherStrArr[i-1], strArr[i]), key , debug);

            //encrypt block
            String block;
            if(i != 0) {
                block = DES.xor(cipherStrArr[i - 1], strArr[i]);
            } else {
                block = cipherStrArr[0];
            }

            String block1 = block.substring(0,block.length()/2);
            String block2 = block.substring(block.length()/2);
            for (int j = 1; j <= 2; j++) {
                String tmpL = block1;
                block1 = block2; // Li = Ri-1
                block2 = DES.xor(tmpL, DES.f(block2, key.substring((j - 1) * 12, ((j - 1) 
                           * 12) + 12), debug)); // Ri = Li-1 xor f(Ri-1, Ki)
            }
            cipherStrArr[i] = block1 + block2;
        }
        
```

cipherStrArr[] is the collection of ciphertext string blocks and the corresponding plaintext blocks belong to strArr[].

# Problem 4

Code in MillerRabin.java

----

Frustrating because I could not find the function BigInteger.modPow() until the end and fixed all Arithmetic Exceptions and integers that were over MAX_VALUE. 

```java
public static String primality(BigInteger n, int k, int seed) {
        Random rand = new Random(seed);

        BigInteger num = n.subtract(BigInteger.ONE); // num = n - 1
        BigInteger d;
        int r;
        for(int i = 0; true; i++) {
            if(!(num.remainder(BigDecimal.valueOf(Math.pow(2,i)).toBigInteger()).equals(BigInteger.ZERO))) { // num % Math.pow(2,i) != 0
                if(i > 0) {
                    i--;
                }
                r = i;
                d = num.divide(BigDecimal.valueOf(Math.pow(2,i)).toBigInteger());
                break;
            }
        }
        System.out.println(n + " = 2^" + r + " * " + d + " + 1");

        for(int i = 0; i < k; i++) {
            //get a random biginteger between [2, n-2]
            BigInteger a;
            do {
                a = new BigInteger((num.subtract(BigInteger.ONE)).bitLength(), rand);
            } while (a.compareTo((num.subtract(BigInteger.ONE))) >= 0 || a.compareTo(new 
                    BigInteger("2")) < 0 ); // while(a >= n - 2)
            System.out.println("a = " + a);

            //BigInteger x = (a.pow(d)).mod(n);
            BigInteger x = a.modPow(d,n);

            if(x.equals(BigInteger.ONE) || x.equals(num)) {
                continue;
            }

            for(int j = 0; j < r - 1; j++) {
                //x = (x.pow(2)).mod(n);
                x = x.modPow(BigInteger.valueOf(2),n);
                if(x.equals(num)){
                    continue;
                }
            }
            return "composite";

        }
        return "probably prime";

    }
```

Implements the algorithm described in the link

https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test#Miller%E2%80%93Rabin_test

Using BigInteger.modPow() changed the algorithm from hanging for multiple minutes with some examples down to instantly. 

k = iterations

seed is the random seed used

# Running the program

In the builds directory there is a cypto.jar file. Run the file like:

<img src="figures/buildOutput.png"/>

java -jar crypto.jar 1.1

Send the jar an argument to run the corresponding problems code. 
